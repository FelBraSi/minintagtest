import { Component, OnInit } from '@angular/core';
import { Api2Service } from '../../../services/api2.service';

@Component( {
  selector: 'app-view2',
  templateUrl: './view2.component.html',
  styleUrls: ['./view2.component.css']
} )
export class View2Component implements OnInit {


    public name  = "View 2";
    public errorData = "";

    //variables de control de elementos
    public showTable  =  false;
    public loadingData  =  false;
    public requestError  =  false;

    //variables de tabla
    public tableIndex  = [];
    public nData  = [];

    //url endpoint
    private _url: string  =  'http://patovega.com/prueba_frontend/dict.php';


    constructor( public api2Service: Api2Service ) { }

    ngOnInit() {
    }

    private processData( thisObject ){

          let resultObject = [];

          //obtiene parametros de error y success
          this.requestError = !thisObject.success;
          this.errorData = thisObject.error;
          let newData  =  JSON.parse( thisObject.data );

          //log para debugueo
          console.log( "Data recibida ( raw )" );
          console.log( newData );

          //crear listado de todas las letras
          let letterList = "abcdefghijklmnñopqrstuvwxyz";
          this.tableIndex = [];
          for ( let i  =  0; i < letterList.length; i++ ) {
                  this.tableIndex.push( letterList.charAt( i ) );
          }

          //iterar paragrafo de cada item
          newData.forEach( dataItem  => {

            //todo a lower case
            let textLow  =  dataItem.paragraph.toLowerCase();

            //regex filtra letras de forma individual, y numeros enteros
            let regexStr =  textLow.match( /[a-z\u00f1\u00d1]|[0-9]+/g );
            let counterArray = [];

            //inicializa suma de enteros
            counterArray["sum"] = 0;

            //inicializa suma de letras
            this.tableIndex.forEach( item  => {
                  counterArray[item] = 0;
            } );

            //compara letras y numeros, y los cuenta o suma segun sea el caso
            regexStr.forEach( item  => {
                if( isNaN( item ) ){
                  counterArray[item] += 1;
                } else {
                  counterArray["sum"] +=  parseInt( item );
                }

            } );

            //genera un array con los resultados, para ser iterado en la tabla
            let resultArray  = [];
            this.tableIndex.forEach( item => {
                resultArray.push( counterArray[item] );
            } );

            //agrega la suma al array
            resultArray.push( counterArray["sum"] );

            //agrega el array del item a la coleccion de arrays
            resultObject.push( resultArray );
          } );

          //log para debugueo
          console.log( "Data despues de ordenar y contar" );
          console.log( resultObject );

          //asigna coleccion de arrays a variable de la clase
          this.nData = resultObject;

          //cambia estado de elementos de UI
          this.loadingData = false;
          this.showTable = true;
    }

    private errorState( error ){

      //muestra el estado de error al usuario
      this.showTable  =  false;
      this.loadingData  =  false;
      this.requestError  =  true;
      this.errorData = error;
    }

    //obtiene data de endnode
    getData() {
      this.api2Service.getData().subscribe(
        response =>  this.processData(response),
        error =>  this.errorState( error )
      )
    }

}
