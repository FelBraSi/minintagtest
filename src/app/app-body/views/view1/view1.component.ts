import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';

@Component( {
  selector: 'app-view1',
  templateUrl: './view1.component.html',
  styleUrls: ['./view1.component.css']
} )
export class View1Component implements OnInit {

    //variables de vista
    public name = "View 1";
    public nData;
    public stringData;

    //variables de control de elementos
    public showTable  =  false;
    public loadingData  =  false;
    public requestError  =  false;
    public errorData = "";

    constructor( public apiService: ApiService ) { }

    ngOnInit( ) {
    }

    getData() {
      this.apiService.getData().subscribe(
        response =>  this.orderData(response),
        error =>  this.errorState( error )
      )
    }

    private orderData( response ){

        this.requestError = !response.success;
        this.errorData = response.error;
        const data = response.data;
        //muestro mensaje comparativo en consola, solo para debugueo
        console.log( "Resultado JSON antes de sort y count" );
        console.log( data );

        //ordeno de forma numerica todos los numeros recibidos, sin utilizar sort(  )
        let sortedArr  =  new Array();

        /*
          primero guardo el indice original y valores.
          Efectivamente push entrega un indice, pero la
          intencion es registrar este indice antes de ordenar
        */
        for(let i=0;i<data.length;i++){
          sortedArr.push( [i,data[i]] );
        }

        /*
          luego ordeno en orden numerico sin utilizar sort
          el indice original no se perdera al ordenar el Array
        */
        let done  =  false;
        while ( !done ) {
          done  =  true;
          for ( let i  =  1; i < sortedArr.length; i  +=  1 ) {
            if ( sortedArr[i - 1][1] > sortedArr[i][1] ) {
              done  =  false;
              let tmp  =  sortedArr[i - 1];
              sortedArr[i - 1]  =  sortedArr[i];
              sortedArr[i]  =  tmp;
            }
          }
        }

        //muestro mensaje comparativo en consola, solo para debugueo
        console.log( "Resultado JSON despues de ordenar y antes de contar ( indice en arreglo,numero )" );
        console.log( sortedArr );

        //se comienza la busqueda de datos repetidos, y a guardar un conteo de ellos
        const results = [];
        let stringResults = "";
        for ( let i  =  0; i < sortedArr.length; i++ ) {

              //desde el segundo item en adelante
              if( i>0 ){

                //si es distinto al item anterior
                if ( sortedArr[i-1][1] !=  sortedArr[i][1] ) {

                  //armo objeto
                  let resultObjt  =  {value:sortedArr[i][1],count:1,firstIndex:sortedArr[i][0],lastIndex:sortedArr[i][0]};

                  //y luego inserto en arreglo y string
                  results.push( resultObjt );
                  stringResults += `,${sortedArr[i][1]}`;
                }

                /*
                  si es igual al item anterior, aumento contador del ultimo
                  item del arreglo hasta este momento
                */
                else {
                  results[results.length-1].count++;

                  //si el valor repetido es el ultimo indice, se informa
                  results[results.length-1].lastIndex = sortedArr[i][0];
                }
              }

              //si es el primer elemento, entonces guardar en arreglo y string
              else {
                let resultObjt  =  {value:sortedArr[i][1],count:1,firstIndex:sortedArr[i][0],lastIndex:sortedArr[i][0]};
                stringResults += `${sortedArr[i][1]}`
                results.push( resultObjt );
              }
        }

        //muestro mensaje comparativo en consola, solo para debugueo
        console.log( "Resultado despues de ordenar y contar" );
        console.log( results );

        //asigno resultado a variables en vista, y habilito la tabla
        this.loadingData = false;
        this.nData =  results;
        this.stringData = stringResults;
        this.showTable = true;
    }

    private errorState( error ){
      console.log( error );
      this.showTable  =  false;
      this.loadingData  =  false;
      this.requestError  =  true;
      this.errorData = error;
    }


}
