import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UppertabComponent } from './uppertab.component';

describe('UppertabComponent', () => {
  let component: UppertabComponent;
  let fixture: ComponentFixture<UppertabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UppertabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UppertabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
