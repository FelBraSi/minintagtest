import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Numbers } from '../models/numbers';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  //url endpoint
  private _url: string  =  'http://patovega.com/prueba_frontend/array.php';

  constructor( private http: HttpClient ) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // API errors
  handleError(error: HttpErrorResponse) {
      // log de error, envia mensaje de error a vista
      console.log(error.message);
      return throwError(  error.message )

  };

  // obtiene data de url 
  getData(): Observable<Numbers> {
    return this.http
      .get<Numbers>(this._url)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }


}
